import React from 'react';
import '../style/profilo.css';
import foto from '../pictures/sardegna.gif';

const Profile = () => {
    return (
        <div className="profilo">

            <div>
                <p>chi siamo:</p>
                <p>costruiamo in tutta <br /> la Sardegna</p>
            </div>

            <div className="middle-section">
                <div>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat ipsam quia quisquam architecto nisi numquam nihil recusandae quibusdam nemo? Neque ad, reiciendis velit earum quisquam ratione sit quidem asperiores blanditiis?Lorem ipsum dolor sit, amet consectetur adipisicing elit. Autem perferendis enim laborum obcaecati veritatis debitis.
                </p>
                </div>
                <div>
                    <img src={foto} alt="foto" />
                </div>
            </div>

            <div className="empty"></div>

        </div>
    )
}
export default Profile;
