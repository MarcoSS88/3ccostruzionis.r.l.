import React, { useState } from "react";
import "../style/gallery.css";
import foto1 from "../pictures/acqua-castello.jpg";
import foto2 from "../pictures/agriturismo-alberi.jpg";
import foto3 from "../pictures/architettura-moderna.jpg";
import foto4 from "../pictures/bungalow-campagna.jpg";
import foto5 from "../pictures/home-real-estate.jpg";
import foto6 from "../pictures/casa-california.jpg";
import foto7 from "../pictures/casa-lago.jpg";
import foto8 from "../pictures/casa-legno.jpg";
import foto9 from "../pictures/miniature.jpg";
import foto10 from "../pictures/villetta-a-schiera.jpg";

const Gallery = () => {
  const [photos1] = useState([foto1, foto2, foto3, foto4, foto5]);
  const [photos2] = useState([foto6, foto7, foto8, foto9, foto10]);

  const createColumn1 = () => {
    return photos1.map((el, index) => {
      return (
        <div key={index}>
          <img src={el} alt="foto" />
        </div>
      );
    });
  };

  const createColumn2 = () => {
    return photos2.map((el, index) => {
      return (
        <div key={index}>
          <img src={el} alt="foto" />
        </div>
      );
    });
  };

  return (
    <div className="gallery">
      <div>
        <p>galleria:</p><p>Usini, Via F.lli Cervi </p>
      </div>

      <div className="pictures-container">
        <div>{createColumn1()}</div>

        <div>{createColumn2()}</div>
      </div>
    </div>
  );
};
export default Gallery;
