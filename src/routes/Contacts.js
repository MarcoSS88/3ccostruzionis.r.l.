import React, { useState } from 'react';
import photo from '../pictures/business-man.jpg';
import '../style/contacts.css';

const Contacts = () => {

    const [content] = useState([
        {
            span: 'person',
            h3: 'amministratore',
            p: ''
        },
        {
            span: 'chat',
            h3: 'contatti',
            p: 'Mobile: (+39) 3297174189'
        },
        {
            span: 'email',
            h3: 'email',
            p: '3ccostruzionisrlss@pec.it'
        },
        {
            span: 'schedule',
            h3: 'disponibilità',
            p: 'Lunedì al Venerdì 09:00 - 18:00'
        }
    ])

    const createContent = () => {
        return content.map((el, index) => {
            return (
                <div key={index}>
                    <span className="material-icons">{el.span}</span>
                    <h3 className="email">{el.h3}</h3>
                    <p>{el.p}</p>
                </div>
            )
        })
    }

    return (
        <div className="contacts">
            <div className="didascalia">
                <p>contattaci:</p>
                <p>richiedi un preventivo <br /> gratuito e senza impegni</p>
            </div>

            <div className="top">
                <div className="left-container">
                    <img src={photo} alt="Luca Chessa" />
                </div>
                <div className="right-container">
                    {createContent()}
                </div>
            </div>

            <div className="pre-footer"></div>
        </div>
    )
}
export default Contacts;

