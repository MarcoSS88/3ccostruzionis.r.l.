import React, { Component } from "react";
import "./style/app.css";
import Header from "./components/Header";
import Home from "./routes/Home";
import Profile from "./routes/Profile";
import Gallery from "./routes/Gallery";
import Contacts from "./routes/Contacts";
import Footer from "./components/Footer";
import GlobalStyle from "./style/global/GlobalStyle";
import { Switch, Route, Redirect } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <GlobalStyle>
        <Route path="/" render={() => <Header />} />
        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <Route path="/profilo" render={() => <Profile />} />
          <Route path="/galleria" render={() => <Gallery />} />
          <Route path="/contatti" render={() => <Contacts />} />
        </Switch>
        <Footer />
        <Redirect to="/" />
      </GlobalStyle>
    );
  }
}
export default App;
