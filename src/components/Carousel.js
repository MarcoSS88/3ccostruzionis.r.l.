import React, { useState, useEffect } from "react";
import "../style/carousel.css";
import gsap from "gsap";
import foto1 from "../pictures/acciaieria.jpg";
import foto2 from "../pictures/artigiano.jpg";
import foto3 from "../pictures/attrezzatura casco.jpg";
import foto4 from "../pictures/costruzione impresa edile.jpg";
import foto5 from "../pictures/persone in edificio in costruzione.jpg";

const Carousel = () => {
  const [images] = useState([foto1, foto2, foto3, foto4, foto5]);
  const [counter, setCounter] = useState(1);
  const [selectedButton, setSelectedButton] = useState(null);

  useEffect(() => {
    if (selectedButton) {
      if (selectedButton === "next") {
        if (counter === 1) {
          gsap.to(`.img${images.length}`, { duration: 1, opacity: 0, ease: "power3.inOut" });
          gsap.to(`.img${counter}`, { duration: 1, opacity: 1, ease: "power3.inOut" });
        } else {
          gsap.to(`.img${counter - 1}`, { duration: 1, opacity: 0, ease: "power3.inOut" });
          gsap.to(`.img${counter}`, { duration: 1, opacity: 1, ease: "power3.inOut" });
        }
      } else {
        if (counter === 1) {
          gsap.to(`.img${counter}`, { duration: 1, opacity: 0, ease: "power3.inOut" });
          gsap.to(`.img${images.length}`, { duration: 1, opacity: 1, ease: "power3.inOut" });
        } else {
          gsap.to(`.img${counter}`, { duration: 1, opacity: 0, ease: "power3.inOut" });
          gsap.to(`.img${counter - 1}`, { duration: 1, opacity: 1, ease: "power3.inOut" });
        }
      }
    }
  }, [selectedButton, counter, images]);

  const goPrevious = () => {
    setSelectedButton("previous");
    if (counter === 1) {
      setCounter(5);
    } else {
      setCounter(counter - 1);
    }
  };

  const goNext = () => {
    setSelectedButton("next");
    if (counter === 5) {
      setCounter(1);
    } else {
      setCounter(counter + 1);
    }
  };

  return (
    <div className="carousel">
      <div className="previous" onClick={() => goPrevious()}>
        {"<"}
      </div>
      <p className="titolo">3c costruzioni</p>
      <p className="frase">costruiamo con passione per voi</p>
      <img className="img1" src={images[0]} alt="foto" />
      <img className="img2" src={images[1]} alt="foto" />
      <img className="img3" src={images[2]} alt="foto" />
      <img className="img4" src={images[3]} alt="foto" />
      <img className="img5" src={images[4]} alt="foto" />
      <div className="next" onClick={() => goNext()}>
        {">"}
      </div>
    </div>
  );
};

export default Carousel;

// componentDidUpdate(_prevProps, prevState) {
//     if (prevState.offset !== this.state.offset) {
//         this.slideCarousel();
//     } else {
//         this.setState({ offset: 0 })
//     }
// }

// getOffset = (value) => {
//     let offset;
//     const images = Array.from(document.querySelectorAll(".image"));
//     for (let image of images) {
//         const position = image.style.transform;
//         offset = +position.substring(position.indexOf("(") + 1, position.indexOf("v"));
//     }
//     if (value === "add") {
//         return offset - 100;
//     } else return offset + 100;
// }

// slideCarousel = () => {
//     const { images } = this.state;
//     const diff = this.state.offset % 100;
//     const maxWidth = +`${images.length - 1}00`;
//     if (this.state.offset >= -maxWidth && this.state.offset <= 0) {
//         gsap.to(".image", { x: `${this.state.offset - diff}vw`, ease: "power3.inOut" })
//     }
// }

// addToCounter = () => this.setState({ offset: this.getOffset("add") })

// diminishCounter = () => this.setState({
//     counter: this.state.counter - 1, offset: this.getOffset()
// })

// showImages = () => {
//     const { images } = this.state;
//     return images.map((el, index) => {
//         return (
//             <div key={index} className="image">
//                 <div>
//                     <img src={el} alt="image" />
//                 </div>
//             </div>
//         )
//     })
// }
