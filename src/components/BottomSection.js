import React from 'react';
import '../style/bottomSection.css';

const BottomSection = () => {
    return (
        <div className="bottom-container">

            <div className="title-lastSection">
                <p>come lavoriamo:</p>
                <p>più che costruttori <br /> noi siamo i vostri assistenti</p>
            </div>

            <div className="lastSection">

                <div className="central-square">
                    <div>
                        <div className="num-romano">I</div>
                        <div className="caption">
                            <p>Opere di edilizia civile e industriale</p>
                            <p>appartamenti, ville, negozi, uffici</p>
                        </div>
                    </div>

                    <div>
                        <div className="num-romano">II</div>
                        <div className="caption">
                            <p>___Titolo in sospeso___</p>
                            <p>interventi di manutenzione ordinaria, straordinaria e ristrutturazione</p>
                        </div>
                    </div>

                    <div>
                        <div className="num-romano">III</div>
                        <div className="caption">
                            <p>___Titolo in sospeso___</p>
                            <p>arredamenti, pavimenti, finiture, illuminazione</p>
                        </div>
                    </div>

                    <div>
                        <div className="num-romano">IV</div>
                        <div className="caption">
                            <p>Recupero edifici</p>
                            <p>ristrutturazioni e restauri</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}

export default BottomSection;
