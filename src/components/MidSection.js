import React from 'react';
import '../style/midSection.css';

const MidSection = () => {
    return (
        <div className="middle">
            <div className="quote1">
                <p>" Siate come i giardinieri, investite le vostre energie e i vostri talenti in modo tale che qualsiasi cosa fate duri una vita
                intera o perfino più a lungo. "</p>
                <p>Sergio Marchionne</p>
            </div>
            <div className="quote2">
                <p>“ Mi piacciono le cose belle e ben fatte. Ritengo addirittura che estetica ed etica si equivalgano. Le cose belle sono etiche,
                mentre le cose non etiche non sono belle. ”</p>
                <p>Gianni Agnelli</p>
            </div>
        </div>
    )
}
export default MidSection;
