import React, { useState } from 'react';
import '../style/menu.css';
import { Link } from "react-router-dom";

const Menu = ({ setMobileMenu }) => {

    const [routes] = useState(['home', 'profilo', 'galleria', 'contatti']);

    const menu = () => {
        return routes.map((route, index) => {
            if (route === 'home') {
                return (
                    <li key={index} onClick={() => setMobileMenu(false)}>
                        <Link to={`/`}><p>{route}</p></Link>
                    </li>
                )
            } else return (
                <li key={index} onClick={() => setMobileMenu(false)}>
                    <Link to={`/${route}`}><p>{route}</p></Link>
                </li>
            )
        })
    }

    return (
        <div className="menu">
            <ul>
                {menu()}
            </ul>
        </div>
    )
}
export default Menu;
