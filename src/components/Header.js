import React, { useState, useEffect } from 'react';
import '../style/header.css';
import gsap from 'gsap';
import Menu from './Menu';
import { Link } from "react-router-dom";
import logo from '../Logo/logo color.png';

const Header = () => {

    const [routes] = useState(['home', 'profilo', 'galleria', 'contatti']);
    const [mobileMenu, setMobileMenu] = useState(null);

    const navbar = () => {
        return routes.map((route, index) => {
            if (route === 'home') {
                return (
                    <div key={index}>
                        <Link to={`/`}><p>{route}</p></Link>
                    </div>
                )
            } else return (
                <div key={index}>
                    <Link to={`/${route}`}><p>{route}</p></Link>
                </div>
            )
        })
    }

    useEffect(() => {
        if (mobileMenu) {
            gsap.to(".app", {
                duration: 1,
                y: "+=20rem",
                ease: "power4.inOut"
            });
            gsap.to(".close", {
                duration: 0.2,
                rotation: "-=180",
                ease: "power1.out"
            });

        } else if (mobileMenu === false) {
            gsap.to(".app", {
                duration: 1,
                y: "-=20rem",
                ease: "power4.inOut"
            });
            gsap.to(".hamburger", {
                duration: 0.2,
                rotation: "+=180",
                ease: "power1.out"
            });
        }

    }, [mobileMenu]);

    const showIcon = () => {

        if (!mobileMenu) {
            return <span className="material-icons hamburger" onClick={() => setMobileMenu(true)}>menu</span>
        } else {
            return <span className="material-icons close" onClick={() => setMobileMenu(false)}>clear</span>
        }
    }

    return (
        <>
            <Menu setMobileMenu={setMobileMenu} />

            <div className="header" >
                <div className="header-left">
                    <div className="logo-container">
                        <img src={logo} alt="logo" />
                    </div>
                </div>
                <div className="header-right">
                    {navbar()}
                </div>
                {showIcon()}
            </div >
        </>
    )
}
export default Header;
