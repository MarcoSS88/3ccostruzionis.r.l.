import React from "react";
import "../style/mainContent.css";
import Icons from './Icons';
import MidSection from './MidSection';
import BottomSection from './BottomSection';

const MainContent = () => {

  return (
    <div className="mainContent">
      <div className="first-title">
        <p>I nostri valori:</p>
        <p>
          {" "}
            sorprendiamo sempre <br /> i nostri clienti
          </p>
      </div>

      <Icons />
      <MidSection />
      <BottomSection />
    </div>
  )
}
export default MainContent;
