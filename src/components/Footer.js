import React, { useState } from 'react';
import '../style/footer.css';
import { Link } from "react-router-dom";
import logo from '../Logo/logo color.png';

const Footer = () => {

    const header = document.getElementsByClassName('header')
    const [routes] = useState(['home', 'profilo', 'galleria', 'contatti']);

    const navbar = () => {
        return routes.map((route, index) => {
            if (route === 'home') {
                return (
                    <div onClick={() => header[0].scrollIntoView()} key={index}>
                        <Link to={`/`}><p>{route}</p></Link>
                    </div>
                )
            } else return (
                <div onClick={() => header[0].scrollIntoView()} key={index}>
                    <Link to={`/${route}`}><p>{route}</p></Link>
                </div>
            )
        })
    }

    return (
        <div className="footer">
            <div className="up">
                <div className="footer-left">
                    <div className="logo-container">
                        <img src={logo} alt="logo" />
                    </div>
                </div>
                <div className="footer-right">
                    {navbar()}
                </div>
            </div>
            <div className="down">
                <p>3c Costruzioni S.r.l. © 2008 - S.V Maccia d'Agliastru, 41 - 07100 Sassari (SS) - Tel: 3297174189 - 3ccostruzionisrlss@pec.it </p>
            </div>
        </div>
    )
}
export default Footer;
