import React, { Component } from 'react';
import '../style/main.css';
import Carousel from './Carousel';
import MainContent from './MainContent';

class Main extends Component {

    render() {
        return (
            <div className="main">
                <Carousel />
                <MainContent />
            </div>
        )
    }
}
export default Main;