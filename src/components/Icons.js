import React, { useState } from 'react';
import '../style/icons.css';

const Icons = () => {

    const [content] = useState([
        {
            classDiv: 'one',
            span: 'emoji_objects',
            p1: 'creatività',
            p2: 'non si può comprare'
        },
        {
            classDiv: 'two',
            span: 'verified_user',
            p1: 'serietà',
            p2: 'in ogni nostro progetto'
        },
        {
            classDiv: 'three',
            span: 'build',
            p1: 'competenza',
            p2: 'quello di cui avete bisogno'
        },
        {
            classDiv: 'four',
            span: 'grade',
            p1: 'professionalità',
            p2: 'anni di esperienza'
        }
    ])
    const generateIcons = () => {
        return content.map((icon, index) => {
            return (
                <div className={icon.classDiv} key={index}>
                    <span className="material-icons">{icon.span}</span>
                    <p className="key-word">{icon.p1}</p>
                    <p className="quote">{icon.p2}</p>
                </div>
            )
        })
    }

    return (
        <div className="icon-container">
            {generateIcons()}
        </div>
    )
}

export default Icons;
