import React from 'react';

const GlobalStyle = ({ children }) => {
    return (
        <div className="app">
            {children}
        </div>
    )
}
export default GlobalStyle;
